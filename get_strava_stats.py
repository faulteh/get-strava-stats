#!/usr/bin/env python3
"""
Get strava stats using their API
Free account gets 100 api calls per day for free, so perhaps put it in a cron job every hour should work

Make a .env file with the following contents
CLIENT_ID=YOURCLIENTID
CLIENT_SECRET="YOURSECRET"
REFRESH_TOKEN="YOURREFRESHTOKEN"
ATHELETE_ID=YOURID

and optionally
ACCESS_TOKEN_FILE=/alternate/location/to/.accesstoken
STATS_FILE=/alternate/location/to/stats.json

"""
import requests
import os
import json
import time
import sys
from dotenv import load_dotenv

# Strava API details
load_dotenv()
CLIENT_ID = int(os.getenv('CLIENT_ID', 0))
CLIENT_SECRET = os.getenv('CLIENT_SECRET', None)
REFRESH_TOKEN = os.getenv('REFRESH_TOKEN', None)
ATHELETE_ID = os.getenv('ATHELETE_ID', None)
API_URL = 'https://www.strava.com/api/v3'
OAUTH_URL = '%s/oauth/token' % API_URL

TOKEN_FILE = os.getenv('ACCESS_TOKEN_FILE', '.accesstoken')
STATS_FILE = os.getenv('STATS_FILE', 'strava_stats.json')

def load_token():
    if os.path.exists(TOKEN_FILE):
        with open(TOKEN_FILE, 'r') as f:
            infile = f.read()
        token_resp = json.loads(infile)
        if 'expires_at' in token_resp:
            if time.time() < token_resp['expires_at']:
                if 'access_token' in token_resp:
                    print('Token has not expired, returning')
                    return token_resp
    return None

def save_token(token):
    with open(TOKEN_FILE, 'w') as f:
        f.write(json.dumps(token))
        print('Token saved')

def get_token():
    payload = {
        'client_id': CLIENT_ID,
        'client_secret': CLIENT_SECRET,
        'grant_type': 'refresh_token',
        'refresh_token': REFRESH_TOKEN
    }
    response = requests.post(OAUTH_URL, data=payload)
    if response.status_code == 200:
        return response.json()
    return None

def get_athelete_stats(token):
    print('Getting stats')
    ATHELETE_URL = '%s/athletes/%s/stats' % (API_URL, ATHELETE_ID)
    headers = {'Authorization': 'Bearer %s' % token['access_token']}
    response = requests.get(ATHELETE_URL, headers=headers)
    if response.status_code == 200:
        return response.json()
    return None

def save_stats(stats):
    with open(STATS_FILE, 'w') as f:
        f.write(json.dumps(stats))

if __name__ == '__main__':
    token = load_token()
    if token is None:
        token = get_token()
    if token is None:
        print('Cannot get token')
        sys.exit(1)
    stats = get_athelete_stats(token)
    if stats is not None:
        save_stats(stats)
    save_token(token)
