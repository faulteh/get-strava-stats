- Create an API app on developer.strava.com
- Create a .env file with details that are in the header for the .py file in
  this project with your API APP credentials and your athelete ID (which you
  can get if you go to your profile page on strava the ID is in the URL
- Install requirements with pip3 install -f requirements.txt
- Run the .py file
